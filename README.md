# README #

### Wofür ist dieses Repository? ###
Es dient zur evaluation der Programmierkenntnisse neuer potentieller Mitarbeiter. 


### Was muss ich tun? ###

 * Eine Spieler Class, welche die Logik eines Spielers enthält und Spieleingaben ermöglicht (dieser kann dann 2 mal verwendet werden für beide Spieler)
 * Schiffs Klassen, welche ermöglichen, unterschiedliche Schiffe mit unterschiedlichen grössen zu setzen
 * Die Game Mechanik, welche das Spiel ermöglicht, mit abwechselnden Zügen, welche jeweils die korrekten Boards zeigen
 
### Wichtig ###
Es ist ein grösserer Task, wir erwarten nicht, dass du fertig wirst, wir sind jedoch interessiert, wie du vorgehst, wie der Code strukturiert ist und wie weit deine Kenntnisse reichen. Bitte pushe in regelmässigen Abständen (am liebsten immer wenn du wieder etwas beendet hast) den Code auf Bitbucket und schreibe als Kommentar was du getan hast (in ein bis zwei Sätzen)
 
### Falls du schneller bist ###
Falls du widererwarten schneller bist, kannst du noch einen Computer Spieler implementieren. Ein Random Spieler zum Beispiel, welcher zufällig ein Feld auswählt, welches er noch nicht hatte, und dort drauf schiesst.
