package devedis;

public class Board {
    // Board Layout
    private static final char[] Y_NAMES = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
    private static final int[] X_NAMES = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    private static final int SIZE = 10;

    // Field Types
    private static final String EMPTY = " ";
    private static final String MISS = "O";
    private static final String HIT = "X";

    // Board
    private String[][] board = new String[SIZE][SIZE];

    public Board() {
        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                board[y][x] = EMPTY;
            }
        }
    }

    public String[][] getBoard() {
        return board;
    }

    public void placeShip(String shipToken, int shipSize, int x, int y, int direction) throws InvalidPlacementException {
        if (direction == 0) {
            for (int i = x; i < (x + shipSize); i++) {
                if (!board[y][i].equals(EMPTY))
                    throw new InvalidPlacementException("Schiff kann nicht dort platziert werden");
            }
            for (int i = x; i < (x + shipSize); i++) {
                board[y][i] = shipToken;
            }
        }

        if (direction == 1) {
            for (int i = y; i < (y + shipSize); i++) {
                if (!board[i][x].equals(EMPTY))
                    throw new InvalidPlacementException("Schiff kann nicht dort platziert werden");
            }
            for (int i = y; i < (y + shipSize); i++) {
                board[i][x] = shipToken;
            }
        }
    }

    public static void displayBoard(String[][] ownBoard, String[][] enemyBoard) {
        StringBuilder output;
        StringBuilder frame;
        StringBuilder numbers;
        StringBuilder letters;

        char[][] board = new char[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                board[i][j] = enemyBoard[i][j].charAt(0);

        // own board frame (left side)
        output = new StringBuilder("Your Board: ");
        frame = new StringBuilder("  +");
        numbers = new StringBuilder("   ");

        for (int x = 0; x < SIZE; x++) {
            frame.append("---+");
            numbers.append(" ").append(X_NAMES[x]).append("  ");
        }
        output.append("                                     ");

        // enemy board frame (right side)
        output.append("Enemy Board: \n");
        frame.append("        " + "+");
        numbers.append("         ");
        for (int x = 0; x < SIZE; x++) {
            frame.append("---+");
            numbers.append(" ").append(X_NAMES[x]).append("  ");
        }
        frame.append("\n");
        output.append(frame);

        for (int y = 0; y < SIZE; y++) {
            // own board fields (left side)
            letters = new StringBuilder("" + Y_NAMES[y] + " | ");
            for (int x = 0; x < SIZE; x++) {
                letters.append(ownBoard[y][x].substring(0, 1)).append(" | ");
            }

            // enemy board fields (right side)
            letters.append("     ").append(Y_NAMES[y]).append(" | ");
            for (int x = 0; x < SIZE; x++) {

                // hide enemy ships
                if ((board[y][x] != HIT.charAt(0)) && (board[y][x] != MISS.charAt(0)))
                    board[y][x] = EMPTY.charAt(0);

                letters.append(board[y][x]).append(" | ");
            }

            output.append(letters).append("\n").append(frame);
        }

        output.append(numbers);

        // display board
        System.out.println("\n" + output + "\n");
    }
}
