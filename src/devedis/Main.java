package devedis;

/*** Der Rest liegt bei dir. Folgende Teile musst du implementieren:
 *  -   Eine Spieler Class, welche die Logik eines Spielers enthält und Spieleingaben ermöglicht (dieser kann dann 2 mal
 *      verwendet werden für beide Spieler)
 *  -   Schiffs Klassen, welche ermöglichen, unterschiedliche Schiffe mit unterschiedlichen grössen zu setzen
 *  -   Die Game Mechanik, welche das Spiel ermöglicht, mit abwechselnden Zügen, welche jeweils die korrekten Boards zeigen
 *
 *  Es ist ein grösserer Task, wir erwarten nicht, dass du fertig wirst, wir sind jedoch interessiert, wie du vorgehst,
 *  wie der Code strukturiert ist und wie weit deine Kenntnisse reichen. Bitte pushe in regelmässigen Abständen
 *  (am liebsten immer wenn du wieder etwas beendet hast) den Code auf Bitbucket und schreibe als Kommentar was du
 *  getan hast (in ein bis zwei Sätzen)
 *
 *  Falls du schneller bist:
 *  -   Falls du widererwarten schneller bist, kannst du noch einen Computer Spieler implementieren. Ein Random Spieler
 *      zum Beispiel, welcher zufällig ein Feld auswählt, welches er noch nicht hatte, und dort drauf schiesst.
 ***/

public class Main {
    public static void main(String[] args) {
        // Create player boards
        Board myBoard = new Board();
        Board enemyBoard = new Board();

        // Place ship
        try {
            myBoard.placeShip("S", 3, 2, 1, 1);
        } catch (InvalidPlacementException e) {
            System.out.println(e.getMessage());
        }

        // Show boards
        Board.displayBoard(myBoard.getBoard(), enemyBoard.getBoard());
    }
}
